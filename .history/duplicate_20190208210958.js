var parseString = require('xml2js').parseString;
var fs = require('fs');
var invalidFiles = 0;
var offset = 0;

var startIndex = 0, endIndex = 4000;
// readFiles('inputs/', 'files/');

function readFiles(mainName, subDirName) {
  offset = offset + 5;
  const dirname = `${mainName}${subDirName}`;
  fs.readdir(dirname, function (err, filenames) {
    if (err) {
      return;
    }
    filenames.forEach(function (filename, index) {
      if (startIndex <= index && endIndex >= index) {
        fs.readFile(dirname + filename, 'utf-8', function (err, xml) {
          if (err) {
            fs.writeFile(`outputs/${subDirName}/${filename}`, xml, function (err, data) { });
            console.log('File not executed! ', err);
            return;
          }
          parseString(xml, function (err, result) {
            if (err) {
              fs.writeFile(`outputs/faiiled/${filename}`, xml, function (err, data) { });
            } else {
              fs.writeFile(`outputs/success/${filename}`, xml, function (err, data) { });
            }
          });
        });
      }
    });
  });
}

function createFiles() {
  const startIndex = 1930535681500 + 100000;
  fs.readFile('inputs/files/Style.xml', 'utf-8', function (err, xml) {
    parseString(xml, function (err, result) {
      if (err) {
      } else {
        for (let i = 0; i < 5000; i++) {
          const data = startIndex + i;
          const output = xml.replace(/20234100/g, `${data}`);
          fs.writeFile(`outputs/test-05-2-2019/${data}.xml`, output, function (err, data) { });
        }
      }
    });
  });
}

function createEmptyFiles() {
  const startIndex = 1975421 + 170000;
  for (let i = 0; i < 5000; i++) {
    const data = startIndex + i;
    // const output = xml.replace(/20234100/g, `${data}`);
    fs.writeFile(`outputs/emptyfiles/${data}.txt`, `File Number ${data}`, function (err, data) { });
  }
}

createFiles();
// createEmptyFiles();