var parseString = require('xml2js').parseString;
var fs = require('fs');
var invalidFiles = 0;
var offset = 0;

var startIndex = 0, endIndex = 4000;
readFiles('inputs/', 'files/');

function readFiles(mainName, subDirName) {
  offset = offset + 5;
  const dirname = `${mainName}${subDirName}`;
  fs.readdir(dirname, function (err, filenames) {
    if (err) {
      return;
    }
    filenames.forEach(function (filename, index) {
      if (startIndex <= index && endIndex >= index) {
        fs.readFile(dirname + filename, 'utf-8', function (err, xml) {
          if (err) {
            fs.writeFile(`outputs/${subDirName}/${filename}`, xml, function (err, data) { });
            console.log('File not executed! ', err);
            return;
          }
          parseString(xml, function (err, result) {
            if (err) {
              fs.writeFile(`outputs/faiiled/${filename}`, xml, function (err, data) { });
            } else {
              fs.writeFile(`outputs/success/${filename}`, xml, function (err, data) { });
            }
          });
        });
      }
    });
  });
}